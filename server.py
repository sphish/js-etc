import traceback
from socketserver import ThreadingTCPServer, StreamRequestHandler
import json
import threading
import os
import logger

class MyRequestHandler(StreamRequestHandler):
    def handle(self):
        data = self.request.recv(1024)
        jdata = json.loads(data)
        print("Receive data from '%r'"% (data))
        print("Receive jdata from '%r'"% (jdata))
        rec_src = jdata[0]['src']
        rec_dst = jdata[0]['dst']

        cur_thread = threading.current_thread()
        response = [{'thread':cur_thread.name,'src':rec_src,'dst':rec_dst}]

        jresp = json.dumps(response)
        self.request.sendall(jresp.encode())

if __name__ == "__main__":
    host = ""
    port = 9996
    addr = (host, port)

    server = ThreadingTCPServer(addr, MyRequestHandler)
    server.serve_forever()

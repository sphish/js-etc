# ~~~~~==============   HOW TO RUN   ==============~~~~~
# 1) Configure things in CONFIGURATION section
# 2) Change permissions: chmod +x bot.py
# 3) Run in loop: while true; do ./bot.py; sleep 1; done
from __future__ import print_function

import sys
import socket
import time
import json
import logging
from threading import Lock
# ~~~~~============== CONFIGURATION  ==============~~~~~
# replace REPLACEME with your team name!
team_name="GEODUDE"
# This variable dictates whether or not the bot is connecting to the prod
# or test exchange. Be careful with this switch!
test_mode = True

# This setting changes which test exchange is connected to.
# 0 is prod-like
# 1 is slower
# 2 is empty

sellprice = {} # best sell price
buyprice = {} # best buy price

test_exchange_index=0
prod_exchange_hostname="production"

port = 25000 + (test_exchange_index if test_mode else 0)
exchange_hostname = "test-exch-" + team_name if test_mode else prod_exchange_hostname

# ~~~~=============== Order ==================
class Order:
    def __init__(self):
        self.now = 0
        self.mutex = Lock()
    
    def reset(self):
        self.mutex.acquire()
        self.now = 0
        self.mutex.release()

    def getOrder(self):
        self.mutex.acquire()
        ret = self.now
        self.now += 1
        self.mutex.release()
        return ret

# ~~~~~============== NETWORKING CODE ==============~~~~~
def connect():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((exchange_hostname, port))
    return s.makefile('rw', 1)

def send(exchange, obj):
    json.dump(obj, exchange)
    exchange.write("\n")
    logging.warning("send: " + str(obj))

def avg(x) :
    assert(len(x) > 0)
    s = 0
    for i in range(len(x)) :
        s += x[i]
    return float(s) / len(x)

def recv(exchange):
    resp = json.loads(exchange.readline())
    if resp['type'] == 'error':
        logging.error("recv: " + str(resp))
        print("recv: ", resp)
    elif resp['type'] == 'ack':
        logging.warning("recv: " + str(resp))
    else:
        logging.info("recv: " + str(resp))
    return resp

def snr(exchange, obj):
    send(exchange, obj)
    return recv(exchange)

def say_hello(exchange):
    hello = {"type": "hello", "team": "GEODUDE"}
    snr(exchange, hello)

# ===================== Bond handler ==========

class BondHandler:
    
    id_buy_list = []
    id_sell_list = []
    buy = {"type": "add", "order_id": 1, "symbol": "BOND", "dir": "BUY", "price": 999, "size": 99}
    sell = {"type": "add", "order_id": 2, "symbol": "BOND", "dir": "SELL", "price": 1001, "size": 99}
            

    def handle(self, resp, exchange, order):
        if len(self.id_buy_list) == 0:
            id1 = order.getOrder()
            self.buy['order_id'] = id1
            send(exchange, self.buy)
            self.id_buy_list.append(id1)
        if len(self.id_sell_list) == 0:
            id1 = order.getOrder()
            self.sell['order_id'] = id1
            send(exchange, self.sell)
            self.id_sell_list.append(id1)
        # resp = json.loads(msg)
        # resp = msg
        if (resp['type'] == 'fill'):
            for ids in self.id_buy_list:
            # if resp['order_id'] is in self.id_buy_list:
                if ids == resp['order_id']:
                    print('in')
                    counter = order.getOrder()
                    self.buy['order_id'] = counter
                    self.buy['size'] = resp['size']
                    # self.id_buy_list.remove(resp['order_id'])
                    self.id_buy_list.append(counter)
                    # buy = {"type": "add", "order_id": resp['order_id'], "symbol": "BOND", "dir": "BUY", "price": 999, "size": resp['size']}
                    send(exchange, self.buy)
                    return
            for ids in self.id_sell_list:
                if ids == resp['order_id']:
                    # sell = {"type": "add", "order_id": resp['order_id'], "symbol": "BOND", "dir": "SELL", "price": 1001, "size": resp['size']}
                    counter = order.getOrder()
                    self.sell['order_id'] = counter
                    self.sell['size'] = resp['size']
                    # self.id_sell_list.remove(resp['order_id'])
                    self.id_sell_list.append(counter)
                    send(exchange, self.sell)
                    return
        return

def start_logger():
    log_file = "./log"
    logging.basicConfig(filename = log_file, filemode = 'a', level = logging.INFO)

# ===================== Round handler ==========
    
class RoundHandler:
    def handle(self, info, exchange, order_impl):
        if info["type"] == "close":
            order_impl.reset()
        elif info["type"] == "open":
            pass
        if info['type'] == 'book' :
            if len(info['buy']) > 0:            
                buyprice[info['symbol']] = info['buy'][0][0]
            if len(info['sell']) > 0:
                sellprice[info['symbol']] = info['sell'][0][0]
            

# ===================ADR HAndler==========
class AdrHandler:
    def __init__(self):
        self.prc = []
        self.aprc = []
        self.l = 10

    def handle(self, resp, exchange, order_impl):
        if resp['type'] == 'trade' and resp['symbol'] == 'BABA':
            if (len(self.prc) < self.l) :
                self.prc.append(resp['price'])
            else :
                for i in range(self.l - 1, 0, -1) : self.prc[i] = self.prc[i - 1]
                self.prc[0] = resp['price']
        elif resp['type'] == 'trade' and resp['symbol'] == 'AABA':
            if (len(self.aprc) < self.l) :
                self.aprc.append(resp['price'])
            else :
                for i in range(self.l - 1, 0, -1) : self.aprc[i] = self.aprc[i - 1]
                self.aprc[0] = resp['price']

        if (len(self.prc) < 5 or len(self.aprc) < 5):
            return 

        if (avg(self.prc) < sellprice['AABA'] - 1) : # sell
            cnt = order_impl.getOrder()
            testsell = {"type": "add", "order_id": cnt, "symbol": "AABA", "dir": "SELL", "price": sellprice['AABA'] - 1, "size": 1}
            send(exchange, testsell)
        elif (avg(self.prc) > buyprice['AABA'] + 1) : # buy
            cnt = order_impl.getOrder()
            testbuy = {"type": "add", "order_id": cnt, "symbol": "AABA", "dir": "BUY", "price": buyprice['AABA'] + 1, "size": 1}
            send(exchange, testbuy)

# ~~~~~============== MAIN LOOP ==============~~~~~

def main():
    exchange = connect()
    start_logger()
    say_hello(exchange)
    cnt = 0
    
    order_impl = Order() 
    handlers = [RoundHandler(), AdrHandler(), BondHandler()]
    while True:
        try:
            resp = recv(exchange)
            for h in handlers:
                h.handle(resp, exchange, order_impl)
        except Exception as e:
            print(e)
            time.sleep(1)
            continue 

    # A common mistake people make is to call write_to_exchange() > 1
    # time for every read_from_exchange() response.
    # Since many write messages generate marketdata, this will cause an
    # exponential explosion in pending messages. Please, don't do that!

main()
